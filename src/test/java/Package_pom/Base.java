package Package_pom;



import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class Base {
	
	private WebDriver driver;
	private Alert alert = driver.switchTo().alert();
	
	public Base(WebDriver driver ) {
		this.driver = driver;
		
		
	}
	
	public WebDriver chromeDriverConnection() {
		
		System.setProperty("webdriver.chrome.driver","C://Users//jcastiblanco//OneDrive - DXC Production//Documents//Programas descargados//ChromeDriver//Chromedriver.exe");
		driver = new ChromeDriver();
		return driver;
		
	}
	
	public WebElement findelement (By locator) {
		return driver.findElement(locator);
	}
	
	public List<WebElement> findElements(By locator){
		return driver.findElements(locator);
	}
	
	public String getText(WebElement element) {
		return element.getText();
	}
	
	public String getText(By locator) {
		return driver.findElement(locator).getText();
	}
	
	public void type(String inputText, By locator) {
		driver.findElement(locator).sendKeys(inputText);
	}
	
	public void click(By locator) {
		driver.findElement(locator).click();
	}
	
	public boolean isDisplayed(By locator) {
		try {
			return driver.findElement(locator).isDisplayed();
			
		}catch(org.openqa.selenium.NoSuchElementException e) {
		  
			return false;	
		}
	}
	
	public void visit(String url) {
		driver.get(url);
	}
	
	public void implicitWait() {
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	}
	
	public void explicitWait() throws InterruptedException{
	 Thread.sleep(2000);	
	}
	
	public void clear(By locator) {
		driver.findElement(locator).clear();
	}
	
	public void maximaze(){
		driver.manage().window().maximize();		
	}
	
	public void borrarCookies() {
		this.driver.manage().deleteAllCookies();
	}
	
	public void switchToalert() {	
		driver.switchTo().alert();
	}
	
	public void acceptalert(){
		alert.accept();
	}
	
	public void dismissalert() {
		alert.dismiss();
	}
	
	public void gettextalert() {
		alert.getText();
	}
	
	public void typetalert(String algo) {
		alert.sendKeys(algo);
	}
	
	
	
}
