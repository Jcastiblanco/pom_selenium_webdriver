package Package_pom;

import static org.junit.Assert.assertEquals;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class Class_type_page extends Base {
	
	public Class_type_page(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	By ingresosmensuales = By.id("ingresosMensuales");
	By cincoa�os 		 = By.xpath("/html/body/div[3]/div[2]/div/div/div[1]/div[1]/form/div[2]/select/option[1]");
	By dieza�os 		 = By.xpath("//*[@id=\"tabcordion-body-1\"]/div[1]/form/div[2]/select/option[2]");
	By quincea�os 		 = By.xpath("//*[@id=\"tabcordion-body-1\"]/div[1]/form/div[2]/select/option[3]");
	By veintea�os 		 = By.xpath("//*[@id=\"tabcordion-body-1\"]/div[1]/form/div[2]/select/option[4]");
	By mensajeerrormonto = By.xpath("/html/body/div[3]/div[2]/div/div/div[1]/div[1]/form/div[1]/p[1]");
	By buttom 			 = By.xpath("//h4[contains(text(),'Calcular Cr�dito')]");
	By mensajecorrecto   = By.xpath("//body[1]/div[3]/div[2]/div[1]/div[1]/div[1]/div[3]/div[1]/dl[1]/dt[1]");
	By ingresomontoerrado= By.xpath("//body/div[3]/div[2]/div[1]/div[1]/div[1]/div[1]/form[1]/div[1]/p[2]");

	
	
	public String validarmonto() {
		type("400000" , ingresosmensuales);	
		String menssage;
		menssage = getText(mensajeerrormonto);
		
		if(isDisplayed(mensajeerrormonto)) {
			System.out.println("si esta ");			
		}
		else {
			System.out.println("No se encuentra mensaje");
		}
		
		return menssage;
	}
	
	public String caminofeliz5a�os() {
		type("4000000" , ingresosmensuales);
		click(cincoa�os);
		click(buttom);
		String message = getText(mensajecorrecto);
		
		return message;
	}
	
	public String caminofeliz10a�os() {
		type("4000000" , ingresosmensuales);
		click(cincoa�os);
		click(buttom);
		String message = getText(mensajecorrecto);
			
		return message;
	}
	
	public String  caminofeliz15a�os() {
		type("4000000" , ingresosmensuales);
		click(cincoa�os);
		click(buttom);
		String message = getText(mensajecorrecto);
	
		return message;
	}
	
	public String caminofeliz20a�os() {
		type("4000000" , ingresosmensuales);
		click(cincoa�os);
		click(buttom);
		String message = getText(mensajecorrecto);

		return message;
	}
	
	public String validarcampoingresomonto() {
		type("abcdefghijk" , ingresosmensuales);
		String message = getText(ingresomontoerrado);

		return message;
		
	}
	

}
