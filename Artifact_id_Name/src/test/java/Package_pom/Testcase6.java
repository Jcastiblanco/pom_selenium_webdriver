package Package_pom;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

public class Testcase6 {
	
	private WebDriver driver;
	Class_type_page registerpage;

	@Before
	public void setUp() throws Exception {
		registerpage = new Class_type_page(driver);
		driver =  registerpage.chromeDriverConnection();
		registerpage.visit("https://www.metrocuadrado.com/calculadora-credito-hipotecario-vivienda/");
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}

	@Test
	public void test() {
		assertEquals("Por favor ingresar campo",registerpage.validarcampoingresomonto());
	}

}
