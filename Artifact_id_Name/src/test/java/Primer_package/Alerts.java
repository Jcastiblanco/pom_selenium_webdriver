package Primer_package;

import static org.junit.Assert.assertEquals;

import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class Alerts {
	
private WebDriver driver;
String text;
	
	@Before
	public void setUp(){
		System.setProperty("webdriver.chrome.driver","C://Users//jcastiblanco//OneDrive - DXC Production//Documents//Programas descargados//ChromeDriver//Chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://rahulshettyacademy.com/AutomationPractice/");
		
	}	
	
	@Test
	public void Alert1() {			
		driver.findElement(By.id("name")).sendKeys("Jesus Castiblanco");
		driver.findElement(By.id("alertbtn")).click();
		text = driver.switchTo().alert().getText();
		System.out.println(text);
	}
	
	@Test
	public void Alert2() throws InterruptedException {			
		
		driver.findElement(By.id("displayed-text")).sendKeys(Keys.CONTROL + "n");
		
		try {
			Thread.sleep(2000);
			for(String winHandle : driver.getWindowHandles()){
				driver.switchTo().window(winHandle)	;			
			}
			driver.findElement(By.id("input")).sendKeys("Listo");
		}catch(Exception e){
			System.out.println(e);
		}
			
		
		text =  driver.findElement(By.xpath("/html/body/header/div[1]/div/ul/li[2]/a/span")).getText();
		System.out.println(text);
		Thread.sleep(3000);
		
	}
	/*
	@Test
	public void Alert3() {			
		
	}
	
	@Test
	public void Alert4() {			
		
	}*/
	
	@After
	public void tearDown() {
		driver.quit();
	}


}
